# fw-batteryled
fw-batteryled watches your battery progress and adjusts the framework laptops various leds to represent the charge left in your battery. It accomplishes this task by using *ectool* to change the color of one of the laptops three leds depending on whether the battery's percentage could be classified as "high", "medium", or "low"
## Installation

Requirements: cargo/rust installed

Steps:

- Clone this repo

- run "cargo build" or "cargo build --release" depending on whether you would like the debug or release versions respectively

- Decide whether you would like to build ectool yourself or use the provided binary

- If you would like to build ectool yourself:

	1. Run "git submodule update"

	2. Enter the 'util' directory

	3. Execute 'make_ectool.sh'

	4. Run 'install-ectool-built.sh'. This script will install the previously built ectool binary to /opt/fw-batteryled/ectool. In order to use this binary please ensure '/opt/fw-batteryled' is in your PATH.

- Or if you would prefer to use the prebuilt binary instead:

	1. Enter the 'util' directory

	2. Run 'install-ectool.sh' this will install the prebuilt ectool binary to /opt/fw-batteryled/ectool. Please ensure '/opt/fw-batteryled' is in your path so that the daemon will be able to locate it.

- Finally, run install.sh from the project directory. This script will install the systemd service and timer files along with the binary and the default config. Take a look at the script for specifics.

- To begin the timer start or enable fw-batteryled.timer with systemctl. By default the timer executes fw-batteryled.service every 2 minutes to update the led in accordance with the config file	.

  

## Usage

#### General usage
---
The fw-batteryled.timer unit executes fw-batteryled.service every two minutes by default. fw-batteryled.service is a oneshot service that runs '/opt/fw-batteryled/fw-batteryled'. The fw-batteryled binary reads the battery's percentage from '/sys/class/power_supply/BATX/capacity' and adjusts the configured LEDs. How often the timer executes can be adjusted by modifying the OnUnitActiveSec variable in the fw-batteryled.timer unit. It is also possible to not use systemd and instead set up a cron job to execute the binary, I just personally use systemd and thus wrote units for it. Please read the config below to further adjust the service.

#### Config
---
The fw-batteryled binary reads the config located at '/etc/fw-batteryled/config' by default. At this moment the location of the config is not modifiable except by editing the source. 

The config has 7 options by default, all of which __must__ be configured, otherwise the program will panic. 
1. high:
	- Any percentage above this variable will be considered "high" battery
2. low:
	- Any percentage below this variable is considered "low" battery
3. color_high:
	- The color used when your battery is "high"
4. color_med:
	- The color used when your battery is between the "high" and "low" variables
5. color_low:
	- The color used when you battery is "low"
6. batx:
	- The battery to read from in the "/sys/class/power_supply/" directory. __THIS VARIABLE IS CASE SPECIFIC__.
	- To determine what to use here look in the "/sys/class/power_supply/" directory for a folder named something along the lines of "BATX" where "X" is an integer. Usually 0 or 1.
7. led:
	- This variable tells whether the left, power, or right led should be used to indicate battery status.
	- Left and Right are the charging indicator lights on the respective side of the laptop and are thus overridden by the laptop being plugged in
	- Power is the light around the fingerprint sensor and is only reset by a reboot.

#### Note on colors:
- The left and right LEDs support the colors: red, green, yellow, white, amber, and blue
- The power LED does supports all of the colors the left and right do __except__ blue
## Support

Feel free to open an issue on gitlab if you encounter any issues and I will do my best to help. You can also contact me 
## Contributing

I am fairly new to git so I cannot provide the best help here. But do feel free to open a merge request for proposed changes and  I will do my best to review and merge changes.

## Authors and acknowledgment

Me! Thanks me!
Also, I primarily use github so if you want to see my other stuff (you probably don't) check out [my github](https://github.com/erocks26). The only project I really care about over there is eutils so yeah. Though I guess I plan on moving everything to gitlab eventually.