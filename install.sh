mkdir /etc/fw-batteryled -v -p
mkdir /opt/fw-batteryled -v -p
install ./target/release/fw-batteryled /opt/fw-batteryled/fw-batteryled -m 755 -o root -g root -v
install ./defconfig /etc/fw-batteryled/config -m 644 -o root -g root -v
install ./systemd/fw-batteryled.service /usr/lib/systemd/system/fw-batteryled.service -m 644 -o root -g root -v
install ./systemd/fw-batteryled.timer /usr/lib/systemd/system/fw-batteryled.timer -m 644 -o root -g root -v

