systemctl disable --now fw-batteryled.timer
rm /usr/lib/systemd/system/fw-batteryled.timer -v
rm /usr/lib/systemd/system/fw-batteryled.service -v
systemctl daemon-reload
rm -r -v /etc/fw-batteryled/
rm -r -v /opt/fw-batteryled/
