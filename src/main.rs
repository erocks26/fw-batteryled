use std::fs;
use serde::{Deserialize, Serialize};
use std::path::Path;
use std::process::Command;
use clap::Parser;

#[derive(Parser,Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    /// Validate the config located at '/etc/fw-batterystatus/config'
    #[arg(short, long)]
    validateconfig: bool,
}

#[derive(Debug, Deserialize, Serialize)]
struct Config {
    led: String,
    high: u8,
    low: u8,
    color_high: String,
    color_med: String,
    color_low: String,
    batx: String,
}

impl Config {
    fn validate(&self) {
        if (self.high > 100) || (self.high < self.low) {
            println!("Config Invalid!");
            println!("'high' > 'low'");
            std::process::exit(1);
        }
        if (self.low < 1) || (self.low > self.high) {
            println!("Config Invalid!");
            println!("'low' > 'high'");
            std::process::exit(1);
        }
        let batx = &self.batx;
        let path = format!("/sys/class/power_supply/{batx}");
        if !!!Path::new(&path).exists() {
            println!("Config Invalid!");
            println!("Path: '{path}' not accessible");
            std::process::exit(1);
        }
        if &self.led.to_lowercase() as &str == "power" {
            match &self.color_high as &str {
                "red" | "green" | "yellow" | "white" | "amber" => (),
                _ => {
                    println!("Config Invalid!");
                    println!("'color_high' must be a valid color for 'power' led:");
                    println!("'red' 'green' 'yellow' 'white' 'amber'");
                    std::process::exit(1);
                }
            }
            match &self.color_low.to_lowercase() as &str {
                "red" | "green" | "yellow" | "white" | "amber" => (),
                _ => {
                    println!("Config Invalid!");
                    println!("'color_low' must be a valid color for 'power' led:");
                    println!("'red' 'green' 'yellow' 'white' 'amber'");
                    std::process::exit(1);
                }
            }
        } else {
            match &self.color_high.to_lowercase() as &str {
                "red" | "green" | "yellow" | "white" | "amber" | "blue" => (),
                _ => {
                    println!("Config Invalid!");
                    println!("'color_high' must be a valid color for 'left' or 'right' led:");
                    println!("'red' 'green' 'yellow' 'white' 'amber'");
                    std::process::exit(1);
                }
            }
            match &self.color_low.to_lowercase() as &str {
                "red" | "green" | "yellow" | "white" | "amber" | "blue" => (),
                _ => {
                    println!("Config Invalid!");
                    println!("'color_low' must be a valid color for 'left' or 'right' led:");
                    println!("'red' 'green' 'yellow' 'white' 'amber'");
                    std::process::exit(1);
                }
            }
        }
        match &self.led.to_lowercase() as &str{
            "left" | "right" | "power" => (),
            _ => {
                println!("Config Invalid!");
                println!("'led' must be 'left', 'right', or 'power'");
                std::process::exit(1);
            }
        }
    }
}

fn main() {
    let config = read_config();
    let args = Args::parse();
    if args.validateconfig {
        config.validate();
        println!("Success! Config valid.");
        std::process::exit(0);
    }
    let percentage = get_percentage(&config.batx);
    if percentage < config.low {
        set_color(1, &config);
    } else if percentage > config.low && percentage < config.high {
        set_color(2, &config);
    } else {
        set_color(3, &config);
    }
}

fn read_config() -> Config {
    let contents = fs::read_to_string("/etc/fw-batteryled/config").expect("Could not read config");
    let serialized: Config = ron::from_str(&contents).unwrap();
    serialized.validate();
    serialized
}

fn get_percentage(batx: &str) -> u8 {
    let path = format!("/sys/class/power_supply/{batx}/capacity");
    let read = fs::read_to_string(Path::new(&path));
    let percent = match read {
        Ok(r) => {
            r
        },
        Err(e) => {
            println!("{:?}", e);
            std::process::exit(2);
        }
    };
    let result: u8 = match strip_trailing_newline(&percent).parse::<u8>() {
        Ok(r) => r,
        Err(e) => {
            println!("{:?}", e);
            std::process::exit(3);
        }
    };
    result
}

fn strip_trailing_newline(input: &str) -> &str {
    input
        .strip_suffix("\r\n")
        .or(input.strip_suffix("\n"))
        .unwrap_or(input)
}

fn set_color(category: u8, config: &Config) {
    match category {
        1 => {
            let result = Command::new("ectool")
                .arg("led")
                .arg(&config.led)
                .arg(&config.color_low)
                .spawn();
            match result {
                Ok(_) => (),
                Err(e) => {
                    println!("Failed to spawn process with error: {:?}", e);
                },
            }
        },
        2 => {
            let result = Command::new("ectool")
                .arg("led")
                .arg(&config.led)
                .arg(&config.color_med)
                .spawn();
            match result {
                Ok(_) => (),
                Err(e) => {
                    println!("Failed to spawn process with error: {:?}", e);
                },
            }
        },
        3 => {
            let result = Command::new("ectool")
                .arg("led")
                .arg(&config.led)
                .arg(&config.color_high)
                .spawn();
            match result {
                Ok(_) => (),
                Err(e) => {
                    println!("Failed to spawn process with error: {:?}", e);
                },
            }
        }
        e => {
            println!("Somehow the internal variable 'category' is not 1, 2, or 3. This should literally be impossible.");
            println!("'category' = {e}");
            std::process::exit(2);
        }
    }
}